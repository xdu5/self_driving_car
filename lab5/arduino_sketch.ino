#include "Adafruit_GPS.h"
#include "PulseOximeter.h"

Adafruit_GPS GPS;

PulseOximeter pox; //vital

//ultrosonic sensor
const int trigPin = 11; 
const int echoPin = 10;
float duration, dist;

//sound sensor
int ledPin=13;
int sensorPin=7;
int val = 0;

void setup() { 
    pinMode(A1, INPUT); //temperature

    //vital
    pinMode(2, INPUT);
    pinMode(3, OUTPUT);

    //ultrosonic sensor
    pinMode(trigPin, OUTPUT); 
    pinMode(echoPin, INPUT);
    digitalWrite(trigPin, LOW); // Initialize trigPin as LOW

    //sound
    pinMode(ledPin, OUTPUT);
    pinMode(sensorPin, INPUT);

    Serial.begin(9600);
}

void loop() { 
    // GPS
    if (Serial.available() > 0) {
      string gpsResult = GPS.read();
      Serial.println(gpsResult);
    } else {
      Serial.println("No data");
    }
    delay(1000);

    //vital
    if (pox.begin()) {
        string spO2_res = pox.getSpO2();
        string hb_res = pox.getHeartRate();
        Serial.println("Oxygen percentage: " + spO2_res + "; Heart rate: " + hb_res);
        digitalWrite(3, HIGH);
    } else {
        Serial.println("No data");
        digitalWrite(3, LOW);
    }
    delay(1000);

    //temperature
    float temp = analogRead(A1) / 1023.0 * 5.0 * 100.0;  // temperature = analog voltage * 100
    Serial.println("temperature: " + to_string(temp));
    delay(1000);

    //ultrasonic Sensor
    digitalWrite(trigPin, HIGH);
    delayMicroseconds(10);
    digitalWrite(trigPin, LOW);
    duration = pulseIn(echoPin, HIGH); // µs
    dist = duration * pow(10, -6) * 34000 / 2; // cm
    Serial.println("Duration: " + to_string(duration) + "µs" + ", Distance: " + to_string(dist) + "cm");
    delay(100);

    //sound sensor
    val = digitalRead(sensorPin);
    if (val==HIGH) {
       digitalWrite(ledPin, HIGH);
       Serial.println(val);
    } else {
       digitalWrite(ledPin, LOW);
    }
    delay(100);

}